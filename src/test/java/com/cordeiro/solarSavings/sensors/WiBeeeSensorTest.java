package com.cordeiro.solarSavings.sensors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;

class WiBeeeSensorTest {

    @Test
    @DisplayName("Test sensor response is not negative")
    void instantWattConsumption() throws SensorException {

        //create

        //instantiate a sensor to unit test
        Sensor myGridSensor = new WiBeeeSensor("2", "GRID_TEST", "http://192.168.1.202/services/user/values.xml?id=WIBEEE.pact");

        assertNotEquals(-1, myGridSensor.instantWattConsumption());


    }
}