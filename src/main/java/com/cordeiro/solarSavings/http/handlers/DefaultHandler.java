package com.cordeiro.solarSavings.http.handlers;

import com.cordeiro.solarSavings.data.SensorStats;
import com.cordeiro.solarSavings.util.HibernateUtil;
import com.cordeiro.solarSavings.util.Log;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.query.Query;

import javax.persistence.TemporalType;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class DefaultHandler implements com.sun.net.httpserver.HttpHandler {

    //Only the HTTP GET is supported. THe UPDATE for the plug management will be included later
    //Also including support to reject other HTTP verbs besides GET

    private static final String _GET = "GET";

    private static final int _ST_OK = 200;
    private static final int _ST_NOT_ALLOWED = 405;

    private static final String _H_ALLOW = "Allow";
    private static final String _H_CONTENT_TYPE = "Content-Type";

    private static final Charset _CHARSET = StandardCharsets.UTF_8;

    //default page
    private static String handleDefaultPath() {

        //built an HTML response
        String mainPageTemplate1 = "<html>\n" +
                "\n" +
                "<head>\n" +
                "<meta http-equiv=Content-Type content=\"text/html; charset=windows-1252\">\n" +
                "\n" +
                "<style>\n" +
                "<!--\n" +
                " /* Font Definitions */\n" +
                " @font-face\n" +
                "\t{font-family:\"Cambria Math\";\n" +
                "\tpanose-1:2 4 5 3 5 4 6 3 2 4;}\n" +
                "@font-face\n" +
                "\t{font-family:Calibri;\n" +
                "\tpanose-1:2 15 5 2 2 2 4 3 2 4;}\n" +
                " /* Style Definitions */\n" +
                " p.MsoNormal, li.MsoNormal, div.MsoNormal\n" +
                "\t{margin-top:0cm;\n" +
                "\tmargin-right:0cm;\n" +
                "\tmargin-bottom:8.0pt;\n" +
                "\tmargin-left:0cm;\n" +
                "\tline-height:107%;\n" +
                "\tfont-size:11.0pt;\n" +
                "\tfont-family:\"Calibri\",sans-serif;}\n" +
                ".MsoChpDefault\n" +
                "\t{font-family:\"Calibri\",sans-serif;}\n" +
                ".MsoPapDefault\n" +
                "\t{margin-bottom:8.0pt;\n" +
                "\tline-height:107%;}\n" +
                "@page WordSection1\n" +
                "\t{size:595.3pt 841.9pt;\n" +
                "\tmargin:70.85pt 3.0cm 70.85pt 3.0cm;}\n" +
                "div.WordSection1\n" +
                "\t{page:WordSection1;}\n" +
                "-->\n" +
                "</style>\n" +
                "\n" +
                "</head>\n" +
                "\n" +
                "<body style='word-wrap:break-word'>\n" +
                "\n" +
                "<div class=WordSection1>\n" +
                "\n" +
                "<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US>HOME SOLAR SAVINGS MANAGEMENT</span></p>\n" +
                "<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US>&nbsp;</span></p>\n" +
                "\n" +
                "<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US>Enable auto on/off THERMO PLUG</span></p>\n" +
                "\n" +
                "<p class=MsoNormal align=center style='text-align:center'><span lang=EN-US>Statistics for the last 60 minutes</span></p>\n" +
                "\n" +
                "<table class=MsoTableGrid align=center border=1 cellspacing=0 cellpadding=0\n" +
                " style='border-collapse:collapse;border:none'>\n" +
                " \n" +
                " <tr>\n" +
                "  <td width=168 valign=top style='width:126.25pt;border:solid windowtext 1.0pt;\n" +
                "  padding:0cm 5.4pt 0cm 5.4pt'>\n" +
                "  <p class=MsoNormal align=center style='margin-bottom:0cm;text-align:center;\n" +
                "  line-height:normal'><span lang=EN-US>Time</span></p>\n" +
                "  </td>\n" +
                "  <td width=196 valign=top style='width:147.25pt;border:solid windowtext 1.0pt;\n" +
                "  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>\n" +
                "  <p class=MsoNormal align=center style='margin-bottom:0cm;text-align:center;\n" +
                "  line-height:normal'><span lang=EN-US>SOLAR production</span></p>\n" +
                "  </td>\n" +
                "  <td width=202 valign=top style='width:151.2pt;border:solid windowtext 1.0pt;\n" +
                "  border-left:none;padding:0cm 5.4pt 0cm 5.4pt'>\n" +
                "  <p class=MsoNormal align=center style='margin-bottom:0cm;text-align:center;\n" +
                "  line-height:normal'><span lang=EN-US>GRID consumption</span></p>\n" +
                "  </td>\n" +
                " </tr>\n" +
                " \n";

        String mainPageTemplate2 = " \n" +
                "</table>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>\n";


        StringBuffer finalHtmlDoc = new StringBuffer();

        //built result table with the last 12 hours resultset
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            Query getData = session.createQuery("from com.cordeiro.solarSavings.data.SensorStats where timestamp >= ?1 " +
                    "and origin<>3 order by timestamp desc", SensorStats.class);

            getData.setParameter(1, Instant.now().atZone(ZoneId.of("Europe/Lisbon")).minusHours(12), TemporalType.DATE);

            List<SensorStats> stats = getData.list();

            String col1 = null;
            String col2 = null;

            for (SensorStats statsRow : stats) {

                //populate SOLAR column first
                if (statsRow.getOrigin() == 1) {

                    col1 = " <tr>\n" +
                            "  <td width=168 valign=top style='width:126.25pt;border:solid windowtext 1.0pt;\n" +
                            "  border-top:none;padding:0cm 5.4pt 0cm 5.4pt'>\n" +
                            "  <p class=MsoNormal align=center style='margin-bottom:0cm;text-align:center;\n" +
                            "  line-height:normal'><span lang=EN-US>" + statsRow.getTimestamp().truncatedTo(ChronoUnit.SECONDS) + "</span></p>\n" +
                            "  </td>\n" +
                            "  <td width=196 valign=top style='width:147.25pt;border-top:none;border-left:\n" +
                            "  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n" +
                            "  padding:0cm 5.4pt 0cm 5.4pt'>\n" +
                            "  <p class=MsoNormal align=center style='margin-bottom:0cm;text-align:center;\n" +
                            "  line-height:normal'><span lang=EN-US>" + statsRow.getWatt() + "</span></p>\n" +
                            "  </td>\n";

                } else if (statsRow.getOrigin() == 2) {

                    col2 = "  <td width=202 valign=top style='width:151.2pt;border-top:none;border-left:\n" +
                            "  none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;\n" +
                            "  padding:0cm 5.4pt 0cm 5.4pt'>\n" +
                            "  <p class=MsoNormal align=center style='margin-bottom:0cm;text-align:center;\n" +
                            "  line-height:normal'><span lang=EN-US>" + statsRow.getWatt() + "</span></p>\n" +
                            "  </td>\n</tr>\n";
                }


                if (!(col1 == null || col2 == null)) {
                    finalHtmlDoc.append(col1).append(col2);
                    col1 = null;
                    col2 = null;
                }
            }

            //no longer necessary
            session.close();

        } catch (HibernateException e) {
            Log.logErrorMessage("HTTP_HANDLER", "Unable to generate response : " + e);
        }

        return mainPageTemplate1.concat(finalHtmlDoc.toString()).concat(mainPageTemplate2);
    }

    //generate a JSON string out of a object instance
    //This will be used to implement a gauge on the page with a client side scripting
    private static String getJSONstring() {

        //to implement later. An object instance with data will be passed or createe here
        //let's make a JSON string from this object
        ObjectMapper mapper = new ObjectMapper();
        String jsonString = "[]"; //default to empty

        try {
            jsonString = mapper.writeValueAsString(new Object());
        } catch (JsonProcessingException e) {
            Log.logErrorMessage("HTTP_HANDLER","Unable to create JSON string : " + e);
        }

        return jsonString;
    }

    //handle method implementation
    public void handle(HttpExchange request) {

        Log.logInfoMessage("HTTP_HANDLER", "Received request");

        try {

            Headers headers = request.getResponseHeaders();
            String requestMethod = request.getRequestMethod().toUpperCase();
            String requestPath = request.getRequestURI().getPath();

            String responseBody = "";

            if (requestMethod.equals(_GET)) {

                if (requestPath.equals("/")) responseBody = handleDefaultPath();

                //send the response
                headers.set(_H_CONTENT_TYPE, String.format("text/html; charset=%s", _CHARSET));

                final byte[] rawResponseBody = responseBody.getBytes(_CHARSET);
                request.sendResponseHeaders(_ST_OK, rawResponseBody.length);
                request.getResponseBody().write(rawResponseBody);

            } else {
                Log.logInfoMessage("HTTP_HANDLER", "Unsuported HTTP verb : " + requestMethod + " " + requestPath);

                headers.set(_H_ALLOW, _GET);
                request.sendResponseHeaders(_ST_NOT_ALLOWED, -1);
            }
        } catch (IOException e) {
            Log.logErrorMessage("HTTP_HANDLER", "Unable to process incoming HTTP request : " + e);
        }

    }

}
