package com.cordeiro.solarSavings.http;

public class HttpStatusError extends Exception {

    private final int status;

    public HttpStatusError(int status) {
        this.status = status;
    }

    public String toString() {
        return "NOK HTTP status of " + status;
    }


}
