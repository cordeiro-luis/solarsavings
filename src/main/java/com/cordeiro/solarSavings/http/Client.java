package com.cordeiro.solarSavings.http;

import com.cordeiro.solarSavings.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.time.Duration;

public class Client {

    private Client() {} // make it impossible to instantiate

    public static String getHttpContent(String address) throws IOException, InterruptedException, HttpStatusError {

        // Create value object to hold the URL
        URL url = null;
        try {
            url = new URL(address);
        } catch (MalformedURLException e) {
            Log.logErrorMessage("HTTP_CLIENT", "Unable to create HTTP client request : " + e);
        }

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setConnectTimeout(5000);

        // This line makes the request
        InputStream responseStream = connection.getInputStream();

        int responseCode = connection.getResponseCode();
        if (responseCode != 200) throw new HttpStatusError(responseCode);

        String s = new String(responseStream.readAllBytes(), StandardCharsets.UTF_8);

        //we could skip this, but to prevent more issues with this sensor, let's just disconnect everytime
        connection.disconnect();

        return s;

    }

    // do the HTTP GET request using the java 11 HTTPClient. This seems to have a bug on java 11
    //so I downgraded for the old "java.net.HttpURLConnection". This was unexpected.
    public static String getHttpContent2(String address) throws IOException, InterruptedException, HttpStatusError {

        //sample taken out and adapted from the API documentation
        HttpClient client = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_1_1)
                .followRedirects(HttpClient.Redirect.NEVER)
                .connectTimeout(Duration.ofSeconds(5))
                .build();

        //create a request builder
        HttpRequest.Builder builder = HttpRequest.newBuilder();
        builder.uri(URI.create(address));
        HttpRequest request = builder.build();

        //Receive the HTTP Response
        HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());

        //evaluate if status 200 was received, if not throw exception
        if (response.statusCode() != 200) throw new HttpStatusError(response.statusCode());

        return response.body();

    }

}
