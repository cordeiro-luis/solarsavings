package com.cordeiro.solarSavings.http;

import com.cordeiro.solarSavings.http.handlers.DefaultHandler;
import com.cordeiro.solarSavings.util.Log;
import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.net.InetSocketAddress;


public class Listener {

    //my local server
    private static HttpServer server;
    private final int port;


    public Listener(int port) {
        this.port = port;

    }

    //public method to allow starting the server from other classes
    public static int startHTTPServer() {

        try {

            server = HttpServer.create();
            server.bind(new InetSocketAddress("localhost", 8080), 2);

        } catch (IOException e) {
            Log.logErrorMessage("HTTP_SERVER", "Unable to start http listener: " + e);
            return -1;
        }

        Log.logInfoMessage("HTTP_SERVER", "HTTP Listener Started. Accepting Requests: " + server.getAddress());

        //define a context for the incoming requests. Using a default path that fits all options
        HttpContext defaultContext = server.createContext("/");

        //set the handler that will process the incoming requests
        defaultContext.setHandler(new DefaultHandler());

        //Starts this server in a new background thread (from API docs)
        server.start();

        //exit program in a clean way.
        HttpServer finalServer = server;

        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    Log.logInfoMessage("HTTP_SERVER", "Stopping HTTP listener");
                    finalServer.stop(2); //it will respond to pending requests and close the socket
                })
        );

        return 0;

    }

    //allows stopping the HTTP server from other classes
    public static void stopHTTPServer() {

        //stop the HTTP server
        server.stop(1);

    }

}
