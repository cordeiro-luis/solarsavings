package com.cordeiro.solarSavings.plugs;

public interface Plug {

    byte getPlugId();

    String getPlugName();

    String getPlugUrl();

    void on();

    void off();

    boolean isON();

    short instantWattConsumption();

    String toString();
}
