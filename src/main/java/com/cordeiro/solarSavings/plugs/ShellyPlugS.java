package com.cordeiro.solarSavings.plugs;

import org.jetbrains.annotations.NotNull;

import static com.cordeiro.solarSavings.util.Log.logInfoMessage;

/////
//This is the Shelly plug S model that provides a http REST interface
//
// The plugh has not arrived yet, so the actual implementation is not done
// but, simulated
////
public class ShellyPlugS implements Plug {

    private byte plugId;
    private String plugName;
    private String plugUrl;

    private boolean plugStatus;


    private ShellyPlugS() {
    }

    //impossible to instantiate with no details. Make construtor private

    public ShellyPlugS(String @NotNull ... plugDetail) {

        this.plugId = Byte.valueOf(plugDetail[0]);
        this.plugName = plugDetail[1];
        this.plugUrl = plugDetail[2];

    }

    @Override
    public byte getPlugId() {
        return plugId;
    }

    @Override
    public String getPlugName() {
        return plugName;
    }

    @Override
    public String getPlugUrl() {
        return plugUrl;
    }

    @Override
    public void on() {
        plugStatus = true;
        logInfoMessage(plugName, "Plug state changed to ON");
    }

    @Override
    public void off() {
        plugStatus = false;
        logInfoMessage(plugName, "Plug state changed to OFF");
    }

    @Override
    public boolean isON() {
        //to be changed for an API call
        return plugStatus;
    }

    @Override
    public short instantWattConsumption() {
        return 0;
    }

    @Override
    public String toString() {
        return "sensor id: " + this.plugId + " name: " + this.plugName + " sensor endpoint: " + this.plugUrl;
    }

}
