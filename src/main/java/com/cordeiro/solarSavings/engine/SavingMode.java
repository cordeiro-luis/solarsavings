package com.cordeiro.solarSavings.engine;

import com.cordeiro.solarSavings.data.SensorStats;
import com.cordeiro.solarSavings.plugs.Plug;
import com.cordeiro.solarSavings.sensors.Sensor;
import com.cordeiro.solarSavings.sensors.SensorException;
import com.cordeiro.solarSavings.util.HibernateUtil;
import com.cordeiro.solarSavings.util.Log;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import static com.cordeiro.solarSavings.util.Log.logInfoMessage;

public class SavingMode implements Runnable {

    private final Sensor solar;
    private final Sensor grid;
    private final Plug plug;

    public SavingMode(Sensor solar, Sensor grid, Plug plug) {
        this.solar = solar;
        this.grid = grid;
        this.plug = plug;
    }

    ////
    // method that is called periodically to decide if the device is turned on or off.
    // many devices can be controlled by different sensors and threads
    ////
    @Override
    public void run() {

        logInfoMessage("SavingModeThread", "retrieve telemetry and calculate saving mode");

        // do some calculations including a threashold to avoid quick on/off cycles
        short solarW = 0;
        short gridW = 0;

        try {
            solarW = solar.instantWattConsumption();
            gridW = grid.instantWattConsumption();
            short plugW = plug.instantWattConsumption();

            //save stats on dB
            //solar
            SensorStats instantSolarStats = new SensorStats();
            instantSolarStats.setOrigin(solar.getSensorId());
            instantSolarStats.setWatt(solarW);

            //grid
            SensorStats instantGridStats = new SensorStats();
            instantGridStats.setOrigin(grid.getSensorId());
            instantGridStats.setWatt(gridW);

            //plug
            SensorStats instantPlugStats = new SensorStats();
            instantPlugStats.setOrigin(plug.getPlugId());
            instantPlugStats.setWatt(plugW);


            Transaction transaction = null;
            Session mainSession = null;

            try (Session session = HibernateUtil.getSessionFactory().openSession()) {

                mainSession = session;

                // start a transaction
                transaction = session.beginTransaction();

                // save the stats objects
                session.save(instantSolarStats);
                session.save(instantGridStats);
                session.save(instantPlugStats);

                // commit transaction
                transaction.commit();

            } catch (HibernateException e) {

                Log.logErrorMessage("SavingModeThread", "Unable to save Statistics: " + e.getMessage());

                if (transaction != null) {
                    transaction.rollback();
                }

            } finally {
                if (mainSession != null) mainSession.close();
            }

        } catch (SensorException e) {
            Log.logErrorMessage("SavingModeThread", e.toString());
        }

        //make a decision, based on the plug active status
        //that changes the calculation
        //This algorithm can be a lot smarter
        if (plug.isON()) {
            int difference = gridW - solarW;
            if (difference > 1600) plug.off();
        } else {
            int difference = solarW - gridW;
            if (difference >= 400) plug.on();
        }

        logInfoMessage("SavingModeThread", "completed - retrieve telemetry and calculate saving mode");

    }
}
