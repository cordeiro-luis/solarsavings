package com.cordeiro.solarSavings.data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "sensorstats", indexes = {
        @Index(name = "idx_sensorstats_timestamp", columnList = "timestamp")
})
public class SensorStats {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "origin", nullable = false)
    private byte origin;

    @Column(name = "watt", nullable = false)
    private short watt;

    @Column(name = "timestamp", nullable = false, updatable = false)
    private Instant timestamp;


    //getters and setters

    public byte getOrigin() {
        return origin;
    }

    public void setOrigin(byte origin) {
        this.origin = origin;
    }

    public Long getId() {
        return id;
    }

    public short getWatt() {
        return watt;
    }

    public void setWatt(short watt) {
        this.watt = watt;
    }

    public Instant getTimestamp() {
        return timestamp;
    }


    @PrePersist
    protected void onCreate() {
        timestamp = Instant.now();
    }


}