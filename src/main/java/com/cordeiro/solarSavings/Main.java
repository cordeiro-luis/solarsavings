package com.cordeiro.solarSavings;

import com.cordeiro.solarSavings.data.Devices;
import com.cordeiro.solarSavings.engine.SavingMode;
import com.cordeiro.solarSavings.http.Listener;
import com.cordeiro.solarSavings.plugs.Plug;
import com.cordeiro.solarSavings.plugs.ShellyPlugS;
import com.cordeiro.solarSavings.sensors.Sensor;
import com.cordeiro.solarSavings.sensors.WiBeeeSensor;
import com.cordeiro.solarSavings.util.HibernateUtil;
import com.cordeiro.solarSavings.util.Log;
import org.hibernate.Session;

import javax.persistence.NoResultException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static com.cordeiro.solarSavings.util.Log.logErrorMessage;
import static com.cordeiro.solarSavings.util.Log.logInfoMessage;

public class Main {

    //Constants
    private static final String _SOLAR = "SOLAR";
    private static final String _GRID = "GRID";
    private static final String _PLUG = "PLUG_THERMO";

    //create a pool of threads to perform the two tests
    private static final ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);

    public static void main(final String[] args) throws Exception {

        initiateSensorTelemetry();
        initateHTTPListner();


        //close DB in a clean way.
        Runtime.getRuntime().addShutdownHook(
                new Thread(() -> {
                    Log.logInfoMessage("MAIN", "Stopping Database ");
                    HibernateUtil.shutdown();
                })
        );

    }

    private static void initiateSensorTelemetry() {

        logInfoMessage("MAIN", "initiate Sensor telemetry");

        //create DB session to fetch configurations
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {

            //retrieve Solar sensor
            logInfoMessage("MAIN", "retrieve Solar sensor settings");
            Devices device = session.createQuery("from com.cordeiro.solarSavings.data.Devices where name='" + _SOLAR + "'", Devices.class).getSingleResult();
            Sensor solar = new WiBeeeSensor(String.valueOf(device.getId()), _SOLAR, device.getUrl());
            logInfoMessage("MAIN", solar.toString());

            //retrieve Grid sensor
            logInfoMessage("MAIN", "retrieve Grid sensor settings");
            device = session.createQuery("from com.cordeiro.solarSavings.data.Devices where name='" + _GRID + "'", Devices.class).getSingleResult();
            Sensor grid = new WiBeeeSensor(String.valueOf(device.getId()), _GRID, device.getUrl());
            logInfoMessage("MAIN", grid.toString());

            //retrieve Plug sensor
            logInfoMessage("Main", "retrieve Plug sensor settings");
            device = session.createQuery("from com.cordeiro.solarSavings.data.Devices where name='" + _PLUG + "'", Devices.class).getSingleResult();
            Plug plug = new ShellyPlugS(String.valueOf(device.getId()), _PLUG, device.getUrl());
            logInfoMessage("MAIN", plug.toString());

            session.close();

            //create executor to retrieve data every 15 seconds
            SavingMode automation = new SavingMode(solar, grid, plug);

            executor.scheduleAtFixedRate(automation, 10, 30, TimeUnit.SECONDS);

        } catch (NoResultException e) {
            logErrorMessage("MAIN", e.getMessage());
        }


    }

    static private void initateHTTPListner() {
        Listener.startHTTPServer();
    }

}