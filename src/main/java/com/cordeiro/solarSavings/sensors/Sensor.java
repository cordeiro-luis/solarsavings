package com.cordeiro.solarSavings.sensors;

public interface Sensor {

    byte getSensorId();

    String getSensorName();

    String getSensorUrl();

    short instantWattConsumption() throws SensorException;

    String toString();
}
