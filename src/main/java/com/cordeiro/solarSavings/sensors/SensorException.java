package com.cordeiro.solarSavings.sensors;

public class SensorException extends Exception {

    public SensorException() {
    }

    public String toString() {
        return "Sensor Error";
    }

}
