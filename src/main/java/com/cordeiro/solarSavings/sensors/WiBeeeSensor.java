package com.cordeiro.solarSavings.sensors;

import com.cordeiro.solarSavings.http.HttpStatusError;
import com.cordeiro.solarSavings.util.Log;
import org.jetbrains.annotations.NotNull;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.IOException;
import java.io.StringReader;

import static com.cordeiro.solarSavings.http.Client.getHttpContent;

public class WiBeeeSensor implements Sensor {

    private byte sensorId;
    private String sensorName;
    private String sensorUrl;

    private WiBeeeSensor() {
    } //impossible to instantiate with no details. Make constructor private

    public WiBeeeSensor(String @NotNull ... sensorDetail) {
        this.sensorId = Byte.valueOf(sensorDetail[0]);
        this.sensorName = sensorDetail[1];
        this.sensorUrl = sensorDetail[2];
    }

    @Override
    public byte getSensorId() {
        return sensorId;
    }

    @Override
    public String getSensorName() {
        return sensorName;
    }

    @Override
    public String getSensorUrl() {
        return sensorUrl;
    }

    @Override
    public short instantWattConsumption() throws SensorException {

        //do http GET request to get an XML from the WiBeeeSensor
        String response;

        try {
            response = getHttpContent(sensorUrl);
        } catch (IOException e) {
            Log.logErrorMessage(sensorName, "Unable to connect to the sensor's HTTP server: " + e);
            throw new SensorException();

        } catch (InterruptedException e) {
            Log.logErrorMessage(sensorName, "Connection to the sensor's HTTP server interrupted: " + e);
            throw new SensorException();

        } catch (HttpStatusError e) {
            Log.logErrorMessage(sensorName, e.getMessage());
            throw new SensorException();
        }


        //Jackson libraries, although capable to parse XML, are strict
        //in the XML format to represent the java bean.
        //it's possible to still grab a part of an XML, but the code is a bit more complex
        //than anticipated. I opted for a fast XML parser

        XMLInputFactory f = XMLInputFactory.newFactory();
        XMLStreamReader sr = null;

        try {
            sr = f.createXMLStreamReader(new StringReader(response));
            boolean fetchValue = false;

            while (sr.hasNext()) {
                int event = sr.next();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    if (sr.getLocalName().equals("value")) fetchValue = true;
                }

                //after the "value" tag, the event contains the actual text
                if (fetchValue && event == XMLStreamConstants.CHARACTERS) {
                    return Double.valueOf(sr.getText()).shortValue();
                }
            }

        } catch (XMLStreamException e) {
            Log.logErrorMessage(sensorName, "Sensor data is invalid: " + e.getMessage());
            throw new SensorException();
        }

        Log.logErrorMessage(sensorName, "Sensor data is not in the expected format. Returning a default value of zero: " + response);
        return 0; //if no value was found

    }

    @Override
    public String toString() {
        return "sensor id: " + this.sensorId + " name: " + this.sensorName + " sensor endpoint: " + this.sensorUrl;
    }

}
