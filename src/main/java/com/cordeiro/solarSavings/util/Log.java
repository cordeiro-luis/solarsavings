package com.cordeiro.solarSavings.util;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

public class Log {

    public static void logInfoMessage(String module, String message) {
        logMessage("INFO", module, message);

    }

    public static void logErrorMessage(String module, String message) {
        logMessage("ERROR", module, message);
    }

    private static void logMessage(String level, String module, String message) {
        System.out.println(Instant.now().truncatedTo(ChronoUnit.MILLIS) + " - [" + level + "]" + "[" + module + "]" + " " + message);
    }


}
